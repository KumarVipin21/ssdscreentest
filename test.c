/**
 * @file test.c
 * @author vipin kumar (kumar.vipin2191@gmail.com)
 * @brief Custom implementation of malloc fuction
 * @version 0.1
 * @date 2019-02-01
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include <test.h>
#include <stdio.h>

#define HEAP_SIZE (4 * 1024 * 1024)

/* the heap has a size of 4 MiB. */

/* Your task is to implment a dynamic memory allocator */

/* As a simplification, you may assume that all allocations will be */
/* of the size 16 bytes or smaller */

/* As a debugging aid, printf() may be used */

/* Other than printf(), you may not use any C standard library function or any other code */

/* If you need any auxiliary memory storage for your work,*/
/* then that memory storage shall be from this heap buffer only */
/* NOTE that this may reduce the effective heap size from the application's point of view */

static uint8_t heap_buffer[HEAP_SIZE];

typedef struct {
    uint32_t isBlockFree;
    uint32_t size;
} * BlockHeaderPtr, BlockHeader;

/* Heap memory parameters */
static uint8_t * heapStartPtr;
static uint32_t blockCount;
static uint32_t totalAllocSize;
static uint8_t * heapEndPtr;

typedef enum {
    NO_BLOCK = 0,
    NEW_BLOCK
} HeapStatusFlag;

/* You may not use any other dynamic memory allocator in this file */

/* You may not use any memory outside the heap_buffer */


/**
 * @brief 
 *      Allocates memory dynamically from heap buffer
 * Block Header:
    * Block Header has the member variables to track the availabilty of the free blocks
    * and to hold the total size of Block Header + MAX_ALLOC
    Mem Block:
    * This region refers to the allocated memory used by callee which has a fixed lenght 
    
    Structure:
      <-- Block Header --> < Mem Block > <-- Block Header --> < Mem Block >
     ------------------------------------------------------------------------------
    | sizeof(BlockHeader) |  MAX_ALLOC  | sizeof(BlockHeader)|  MAX_ALLOC  | ...
     ------------------------------------------------------------------------------
    <------ BlockHeaderPtr:size -------> <------ BlockHeaderPtr:size ------> 
    ^                     ^             ^                    ^             ^  
    a                     a1            b                    b1            c
    (heapStartPtr)                                                           ... (heapEndPtr)
   

    * if isBlockFree == 0 : Memory block is free to allocate
    * if isBlockFree == 1 : Memory block is already allocated
    
 * @param n
 *      Number of bytes to be allocated
 * @return void*
 *      Void pointer to starting address of allocated memory block
 */
void * custom_malloc(const int32_t n) {
    BlockHeaderPtr blkHeaderPtr = (BlockHeaderPtr) heapStartPtr;
    HeapStatusFlag flag = NO_BLOCK;
    /* Returning NULL if n > MAX_ALLOC or n == 0 */
    if(n > MAX_ALLOC || n == 0)
        return NULL;

    /* Traversing from point a to b to c ... and check for a free block */
    while(heapEndPtr > ( (uint8_t *) blkHeaderPtr + sizeof(BlockHeader) + MAX_ALLOC )) {
        printf("Block Availability: %d, size: %d\n", blkHeaderPtr->isBlockFree, blkHeaderPtr->size);
     
        if(blkHeaderPtr->isBlockFree == 0 && blkHeaderPtr->size == 0) {
            /* Here blkHeaderPtr points to a free block, break from travesing loop */
            flag = NEW_BLOCK;
            break;
        }
        /* Jump from a to b to c ...*/
        blkHeaderPtr = (BlockHeaderPtr)((uint8_t *) blkHeaderPtr + blkHeaderPtr->size);
        printf("blkHeaderPtr step addresses: %p\n", (uint8_t *) blkHeaderPtr);
    }
    
    if(flag == NEW_BLOCK) {
        /* Set the block status to Allocated */
        blkHeaderPtr->isBlockFree = 1;
        /* Set the size variable */
        blkHeaderPtr->size = MAX_ALLOC + sizeof(BlockHeader);

        blockCount++;
        totalAllocSize += MAX_ALLOC;
        return ((uint8_t * )blkHeaderPtr + sizeof(BlockHeader));
    } else {
        printf("No more space available in heap\n");
        return NULL;
    }
}

/**
 * @brief 
 *      Deallocates memory from heap buffer for a given address
 * @param ptr 
 *      Starting address of memory block
 */
void custom_free(const void * ptr) {
    /* ptr points to starting address of memory block, We need header address
     of this memory block to deallocate memory*/
    BlockHeaderPtr blkHeadPtr = (BlockHeaderPtr) ptr;
    blkHeadPtr--;
    printf("Ptr: %p %p\n", (void*) ptr, (void *)blkHeadPtr);
    blkHeadPtr->isBlockFree = 0;
    blockCount--;
    blkHeadPtr->size = 0;
    totalAllocSize = totalAllocSize - MAX_ALLOC;
    return;
}

/* Compiling this file as it is gives some unused variable errors. */

/* Your final code must not give any warnings */

/* You shall not use C++ for this task, */

/* This task must be done using C language only. */

int main(){
    uint8_t * var[5];
    /* Initializing heap memory parameters*/
    heapStartPtr = &heap_buffer[0];             /* Starting address of heap buffer */
    heapEndPtr = heapStartPtr + HEAP_SIZE;      /* End address of heap buffer */
    printf("Heap Starting Address: %p, Heap Ending Address: %p\n\n", heapStartPtr, heapEndPtr);

    var[0] = (uint8_t *) custom_malloc(1);
    printf("Assigned mem addr to var[0]: %p, block count = %d, TotalAllocatedMem: %d\n\n", var[0], blockCount, totalAllocSize);
    /* Testing allocated memory by assigning the some value to variable*/
    * var[0] = 0x12;
    printf("var value: %x\n", * var[0]);

    var[1] = (uint8_t *) custom_malloc(15);
    printf("Assigned mem addr to var[1]:: %p, block count = %d, TotalAllocatedMem: %d\n\n", var[1], blockCount, totalAllocSize);
    /* Free the memory for var[1] */
    custom_free(var[1]);
    printf(" After free for the address %p block count = %d, TotalAllocatedMem: %d\n\n", var[1], blockCount, totalAllocSize);

    var[2] = (uint8_t *) custom_malloc(13);
    printf("Assigned mem addr to var[2]:: %p, block count = %d, TotalAllocatedMem: %d\n\n", var[2], blockCount, totalAllocSize);

    var[3] = (uint8_t *) custom_malloc(10);
    printf("Assigned mem addr to var[3]:: %p, block count = %d, TotalAllocatedMem: %d\n\n", var[3], blockCount, totalAllocSize);

    var[4] = (uint8_t *) custom_malloc(5);
    printf("Assigned mem addr to var[4]:: %p, block count = %d, TotalAllocatedMem: %d\n\n", var[4], blockCount, totalAllocSize);
    
    /* Free the memory for var[0] */
    custom_free(var[0]);
    printf(" After free for the address %p block count = %d, TotalAllocatedMem: %d\n\n", var[0], blockCount, totalAllocSize);

    /* Free the memory for var[2] */
    custom_free(var[2]);
    printf(" After free for the address %p block count = %d, TotalAllocatedMem: %d\n\n", var[2], blockCount, totalAllocSize);

    /* Free the memory for var[3] */
    custom_free(var[3]);
    printf(" After free for the address %p block count = %d, TotalAllocatedMem: %d\n\n", var[3], blockCount, totalAllocSize);

    /* Free the memory for var[4] */
    custom_free(var[4]);
    printf(" After free for the address %p block count = %d, TotalAllocatedMem: %d\n\n", var[4], blockCount, totalAllocSize);

    return 0;
}
