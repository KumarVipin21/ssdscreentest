CFILE=./test.c
INCLDIRS=-I.
CFLAGS=-Wall -Wextra -ansi -pedantic $(INCLDIRS)

test: $(CFILE)
	gcc $(CFILE) $(CFLAGS) -o test

clean:
	rm -rf test
